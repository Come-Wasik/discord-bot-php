# My Discord bot

![Build status](https://shields.io/gitlab/pipeline-status/Come-Wasik/discord-bot-php?branch=master)

## Introduction

This is a bot for personal discord server, that allows to manage birthdays and send a message in a given place when it is time to wish it !

## Installation

You need [Docker](https://www.docker.com/) engine and docker compose.

Run

    docker compose run --rm bot composer install

Copy the laravel environment file then complete it

    cp .env.example .env

+ DISCORD_TOKEN = Your discord app id
+ CHANNEL_CALENDAR_ID = The channel when to read and write (where the bot is supposed to be)
+ ROLE_BIRTHDAY_FOLLOWER = [Optional] The role to notify when there is a birthday (it is a global role)


## Run the bot for developement

Run :

    docker compose run --rm -d db

Then close it.

In order to initialize the database, run two times the following:
(You'll get an error the first time)

    docker compose up

## Run the bot for production

To create the production build, run:

    docker compose run --rm bot php laracord app:build

Enter whatever build version you want.

Copy the .env aside the build like :

    cp .env builds/.env

Finally, in order to run the production executable:

    docker compose -f production.yml up -d

