<?php

namespace App\Services;

use App\Actions\BirthdayService;
use App\Models\Birthday;
use Illuminate\Support\Carbon;
use Laracord\Services\Service;

class CheckTodayBirthday extends Service
{
    /**
     * Boot the service.
     */
    public function boot(): self
    {
        $this->handle();

        return $this;
    }

    /**
     * Handle the service.
     */
    public function handle(): void
    {
        $checker = new BirthdayService();

        $channelId = config('discord.channels.calendar');
        throw_if(null === $channelId, 'The calendar channel id should not be empty');

        // Check the birthday now
        $this->console()->log('Start to watch birthday date.');
        $birthday = $checker->checkTodayBirthday();
        $this->notificateBirthday($birthday, $channelId);

        // Wait tomorrow before the next check (one time)
        $nextDayTimeInSeconds = Carbon::now()->diffInSeconds(Carbon::tomorrow()->addMinute());
        $this->getLoop()->addTimer($nextDayTimeInSeconds, function () use ($checker, $channelId) {
            $birthday = $checker->checkTodayBirthday();
            $this->notificateBirthday($birthday, $channelId);

            // Wait one day in seconds before the next check (every day)
            $nextDayTimeInSeconds = 3600 * 24;
            $this->getLoop()->addPeriodicTimer($nextDayTimeInSeconds, function () use ($checker, $channelId) {
                $birthday = $checker->checkTodayBirthday();
                $this->notificateBirthday($birthday, $channelId);
            });
        });
    }

    public function notificateBirthday(?Birthday $birthday, $channelId): void
    {
        if (null !== $birthday) {
            $roleIdToNotify = config('discord.roles.birthday_follower');

            $this
                ->message()
                ->body($roleIdToNotify ? sprintf('<@&%s>', config('discord.roles.birthday_follower')) : '')
                ->send($this->discord()->getChannel($channelId));

            $this
                ->message()
                ->title(__('Birthday announce'))
                ->content(
                    sprintf(
                        __('You can wish **%s**\'s birthday'),
                        $birthday->username
                    )
                )
                ->send($this->discord()->getChannel($channelId));
        }
    }
}
