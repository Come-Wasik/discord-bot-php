<?php

namespace App\Commands\Birthday;

use App\Actions\BirthdayService;
use App\Models\Birthday;
use Laracord\Commands\Command;

class AddCommand extends Command
{
    /**
     * The command name.
     *
     * @var string
     */
    protected $name = 'birthday:add';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Add a birthday.';

    /**
     * Determines whether the command requires admin permissions.
     *
     * @var bool
     */
    protected $admin = false;

    /**
     * Determines whether the command should be displayed in the commands list.
     *
     * @var bool
     */
    protected $hidden = false;

    /**
     * The command usage.
     *
     * @var string
     */
    protected $usage = 'birthday:add {username} {day} {month}';

    /**
     * Handle the command.
     *
     * @param  \Discord\Parts\Channel\Message  $message
     * @param  array  $args
     * @return ?\React\Promise\ExtendedPromiseInterface
     */
    public function handle($message, $args)
    {
        // Clean arguments
        [$username, $day, $month] = $args;
        $day = (int) $day;
        $month = (int) $month;

        // Checks the given birthday date is valid
        if (! Birthday::isValidDate($day, $month)) {
            return $this
                ->message()
                ->title(__('Add birthday'))
                ->content(__('You should provide a valid date'))
                ->error()
                ->send($message);
        }

        // Checks the given username has not been already taken because of the unique constraint
        if (Birthday::isUsernameTaken($username)) {
            return $this
                ->message()
                ->title(__('Add birthday'))
                ->content(__('The username has been already registered'))
                ->error()
                ->send($message);
        }

        (new BirthdayService)->create($username, $day, $month);

        return $this
            ->message()
            ->title(__('Add birthday'))
            ->content(
                sprintf(
                    __('Birthday of %s added at date %s/%s'),
                    $username,
                    str((string) $day)->padLeft(2, '0')->toString(),
                    str((string) $month)->padLeft(2, '0')->toString(),
                )
            )
            ->send($message);
    }
}
