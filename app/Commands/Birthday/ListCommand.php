<?php

namespace App\Commands\Birthday;

use App\Actions\BirthdayService;
use App\Models\Birthday;
use Laracord\Commands\Command;

class ListCommand extends Command
{
    /**
     * The command name.
     *
     * @var string
     */
    protected $name = 'birthday:list';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'List every birthday.';

    /**
     * Determines whether the command requires admin permissions.
     *
     * @var bool
     */
    protected $admin = false;

    /**
     * Determines whether the command should be displayed in the commands list.
     *
     * @var bool
     */
    protected $hidden = false;

    /**
     * Handle the command.
     *
     * @param  \Discord\Parts\Channel\Message  $message
     * @param  array  $args
     * @return ?\React\Promise\ExtendedPromiseInterface
     */
    public function handle($message, $args)
    {
        $birthdays = (new BirthdayService())->list();

        if ($birthdays->isEmpty()) {
            return $this
                ->message()
                ->title(__('Birthday list'))
                ->content(__('No birthday registered yet'))
                ->warning()
                ->send($message);
        }

        return $this
            ->message()
            ->title(__('Birthday list'))
            ->content(
                $birthdays->map(function (Birthday $birthday) {
                    return sprintf(
                        __('Name: %s - %s/%s'),
                        $birthday->username,
                        str((string) $birthday->day)->padLeft(2, '0')->toString(),
                        str((string) $birthday->month)->padLeft(2, '0')->toString()
                    );
                })->join("\n")
            )
            ->info()
            ->send($message);
    }
}
