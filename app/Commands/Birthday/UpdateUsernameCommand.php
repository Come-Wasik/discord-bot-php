<?php

namespace App\Commands\Birthday;

use App\Actions\BirthdayService;
use App\Models\Birthday;
use Laracord\Commands\Command;

class UpdateUsernameCommand extends Command
{
    /**
     * The command name.
     *
     * @var string
     */
    protected $name = 'birthday:update:username';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Update a birthday from a date.';

    /**
     * Determines whether the command requires admin permissions.
     *
     * @var bool
     */
    protected $admin = false;

    /**
     * Determines whether the command should be displayed in the commands list.
     *
     * @var bool
     */
    protected $hidden = false;

    /**
     * The command usage.
     *
     * @var string
     */
    protected $usage = 'birthday:update:username {day} {month} {username}';

    /**
     * Handle the command.
     *
     * @param  \Discord\Parts\Channel\Message  $message
     * @param  array  $args
     * @return ?\React\Promise\ExtendedPromiseInterface
     */
    public function handle($message, $args)
    {
        // Clean arguments
        [$day, $month, $username] = $args;
        $day = (int) $day;
        $month = (int) $month;

        // Checks the given birthday date is valid
        if (! Birthday::isValidDate($day, $month)) {
            return $this
                ->message()
                ->title(__('Update birthday'))
                ->content(__('You should provide a valid date'))
                ->error()
                ->send($message);
        }

        // Checks an entry with the given username exists
        if (Birthday::isUsernameTaken($username)) {
            return $this
                ->message()
                ->title(__('Update birthday'))
                ->content(__('The username has been already registered'))
                ->error()
                ->send($message);
        }

        // Delete an entry from a username
        $updateCount = (new BirthdayService)->updateUsername($day, $month, $username);

        // Check the delete was successfully performed
        if (0 === $updateCount) {
            return $this
                ->message()
                ->title(__('Update birthday'))
                ->content(sprintf(__('We had an error to update the %s birthday'), $username))
                ->error()
                ->send($message);
        }

        return $this
            ->message()
            ->title(__('Update birthday'))
            ->content(
                sprintf(
                    __('The username %s is associated to the new date %s/%s'),
                    $username,
                    str((string) $day)->padLeft(2, '0')->toString(),
                    str((string) $month)->padLeft(2, '0')->toString(),
                )
            )
            ->send($message);
    }
}
