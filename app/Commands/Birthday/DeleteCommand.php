<?php

namespace App\Commands\Birthday;

use App\Actions\BirthdayService;
use App\Models\Birthday;
use Laracord\Commands\Command;

class DeleteCommand extends Command
{
    /**
     * The command name.
     *
     * @var string
     */
    protected $name = 'birthday:delete';

    /**
     * The command description.
     *
     * @var string
     */
    protected $description = 'Delete a birthday.';

    /**
     * Determines whether the command requires admin permissions.
     *
     * @var bool
     */
    protected $admin = false;

    /**
     * Determines whether the command should be displayed in the commands list.
     *
     * @var bool
     */
    protected $hidden = false;

    /**
     * The command usage.
     *
     * @var string
     */
    protected $usage = 'birthday:delete {username}';

    /**
     * Handle the command.
     *
     * @param  \Discord\Parts\Channel\Message  $message
     * @param  array  $args
     * @return ?\React\Promise\ExtendedPromiseInterface
     */
    public function handle($message, $args)
    {
        [$username] = $args;

        // Checks an entry with the given username exists
        if (! Birthday::isUsernameTaken($username)) {
            return $this
                ->message()
                ->title(__('Delete birthday'))
                ->content(__('The username has not been registered'))
                ->error()
                ->send($message);
        }

        // Delete an entry from a username
        $deleteCount = (new BirthdayService)->deleteFromUsername($username);

        // Check the delete was successfully performed
        if (0 === $deleteCount) {
            return $this
                ->message()
                ->title(__('Delete birthday'))
                ->content(sprintf(__('We had an error to remove the %s birthday'), $username))
                ->error()
                ->send($message);
        }

        return $this
            ->message()
            ->title(__('Delete birthday'))
            ->content(sprintf(__('The user %s has been successfully deleted'), $username))
            ->send($message);
    }
}
