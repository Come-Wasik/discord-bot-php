<?php

namespace App\Actions;

use App\Models\Birthday;
use Illuminate\Database\Eloquent\Collection;

class BirthdayService
{
    public function list(): Collection
    {
        return Birthday::query()->select(['username', 'day', 'month'])
            ->orderBy('month', 'asc')
            ->orderBy('day', 'asc')
            ->get();
    }

    public function create(string $username, int $day, int $month): void
    {
        Birthday::create([
            'username' => $username,
            'day' => $day,
            'month' => $month,
        ]);
    }

    /**
     * Delete a birthday from username.
     */
    public function deleteFromUsername(string $username): int
    {
        return Birthday::query()->where('username', $username)->delete();
    }

    /**
     * Delete a birthday from username.
     */
    public function updateDate(string $username, int $day, int $month): int
    {
        return Birthday::query()->where('username', $username)->update([
            'day' => $day,
            'month' => $month,
        ]);
    }

    /**
     * Delete a birthday from date.
     */
    public function updateUsername(int $day, int $month, string $username): int
    {
        return Birthday::query()
            ->where('day', $day)
            ->where('month', $month)
            ->update([
                'username' => $username,
            ]);
    }

    public function checkTodayBirthday(): ?Birthday
    {
        $now = now();

        // Check the today birthday
        /** @var ?Birthday $birthday */
        return Birthday::query()->where('day', $now->day)->where('month', $now->month)->first();

    }
}
