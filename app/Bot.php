<?php

namespace App;

use Discord\Parts\User\Activity;
use Illuminate\Support\Facades\Route;
use Laracord\Laracord;

class Bot extends Laracord
{
    /**
     * The HTTP routes.
     */
    public function routes(): void
    {
        Route::middleware('web')->group(function () {
            // Route::get('/', fn () => 'Hello world!');
        });

        Route::middleware('api')->group(function () {
            // Route::get('/commands', fn () => collect($this->registeredCommands)->map(fn ($command) => [
            //     'signature' => $command->getSignature(),
            //     'description' => $command->getDescription(),
            // ]));
        });
    }

    /**
     * Actions to run after booting the bot.
     */
    public function afterBoot(): void
    {
        $this->updateBotActivity();
        $this->console()->log(sprintf('Translation locale : %s', config('app.locale')));
    }

    public function updateBotActivity(): void
    {
        /** @var Activity $activity */
        $activity = $this->discord()->factory(Activity::class, [
            'type' => Activity::TYPE_PLAYING,
            'name' => 'with Laracord',
        ]);

        $this->discord()->updatePresence($activity);
    }
}
