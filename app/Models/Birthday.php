<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $username
 * @property int $day
 * @property int $month
 */
class Birthday extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'day',
        'month',
    ];

    /**
     * Checks if a username has been already taken.
     */
    public static function isUsernameTaken(string $username): bool
    {
        return self::query()->where('username', $username)->exists();
    }

    /**
     * Checks if a birthday has a valid date.
     */
    public static function isValidDate(int $day, int $month): bool
    {
        return checkdate($month, $day, 2024);
    }
}
