<?php

use App\Actions\BirthdayService;
use App\Models\Birthday;

it('can use model factory', function () {
    expect(Birthday::exists())->toBeFalse();
    Birthday::factory()->create();

    expect(Birthday::exists())->toBeTrue();
});

it('can list birthdays', function () {
    $service = new BirthdayService();

    expect($service->list())->toBeEmpty();

    Birthday::factory()->create();
    expect($service->list())->not->toBeEmpty();
});

it('can create birthday', function () {
    $username = fake()->firstName();
    $day = fake()->numberBetween(1, 31);
    $month = fake()->numberBetween(1, 12);

    $service = new BirthdayService();
    $service->create($username, $day, $month);

    expect(Birthday::query()->where('username', $username)->where('day', $day)->where('month', $month)->exists())->toBeTrue();
});

it('can delete a birthday from username', function () {
    $birthday = Birthday::factory()->create();

    $service = new BirthdayService();
    $service->deleteFromUsername($birthday->username);

    expect(Birthday::query()->where('id', $birthday->id)->doesntExist())->toBeTrue();
});

it('can update a birthday from username', function () {
    $birthday = Birthday::factory()->state(['day' => 1, 'month' => 1])->create();
    expect($birthday->day)->toBe(1);
    expect($birthday->month)->toBe(1);

    $newDay = fake()->numberBetween(1, 31);
    $newMonth = fake()->numberBetween(1, 12);

    $service = new BirthdayService();
    $service->updateDate($birthday->username, $newDay, $newMonth);

    $birthday->refresh();
    expect($birthday->day)->toBe($newDay);
    expect($birthday->month)->toBe($newMonth);
});

it('can update a birthday from date', function () {
    /** @var Birthday $birthday */
    $birthday = Birthday::factory()->state(['username' => 'Jean-mi'])->create();
    expect($birthday->username)->toBe('Jean-mi');

    $newUsername = fake()->userName();

    $service = new BirthdayService();
    $service->updateUsername($birthday->day, $birthday->month, $newUsername);

    $birthday->refresh();
    expect($birthday->username)->toBe($newUsername);
});
